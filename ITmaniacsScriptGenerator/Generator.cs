﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Collections.Generic;

namespace ITmaniacsScriptGenerator
{
    class Generator
    {
        public const string Company_Name = "CompanyName";

        public const string UserSid = "user_sid";
        public const string CustomKeywords1 = "custom_keywords";
        public const string CustomKeywords2 = "custom_keywords2";
        public const string CustomKeywords3 = "custom_keywords3";
        public const string CustomKeywords4 = "custom_keywords4";

        public const string JobType = "JobType";
        public const string JobId = "external_id";
        public const string JobLocation = "Location_City";
        public const string JobCategory = "JobCategory";

        public const string FileName = "FileName";

        public static void GenerateScript(Dictionary<string, string> KVP, int generationType)
        {
            string SQLQueryString = GenerateSQLQueryString(KVP);

            string _scriptFileContent =
                "using System;\n" +
                "using mshtml;\n" +
                "using VisualWebRipper;\n" +
                "using System.Globalization;\n" +
                "\n" +
                "public class Script\n" +
                "{\n" +
                "	public static bool ExportData(WrExportArguments args)\n" +
                "	{\n" +
                "		try\n" +
                "		{\n" +
                $"			args.Database.SetSqlAndPrepare(\"{SQLQueryString}\");\n" +
                "               \n" +
                $"			WrExportTableReader GlobaljobsReader = args.ExportData.GetTableReader(\"{KVP[Company_Name]}\");\n" +
                "			bool run = true;\n" +
                "\n" +
                "			string strdate = \"\";\n" +
                "			DateTime dt = DateTime.Now;\n" +
                "			try\n" +
                "			{\n" +
                "				while (GlobaljobsReader.Read())\n" +
                "				{\n" +
                "					strdate = \"\";\n" +
                "					try\n" +
                "                    {\n" +
                "						strdate = GlobaljobsReader.GetStringValue(\"date\").ToString();\n" +
                "\n" +
                "						if (strdate != \"\")\n" +
                "						{\n" +
                "							strdate = strdate.ToLower().Replace(\"date posted:\", \"\");\n" +
                "							strdate = strdate.Trim();\n" +
                "							dt = DateTime.ParseExact(strdate, \"dd/MM/yyyy\", CultureInfo.InvariantCulture);\n" +
                "						}\n" +
                "						else\n" +
                "						{\n" +
                "							dt = DateTime.Now;\n" +
                "						}\n" +
                "					}\n" +
                "					catch (Exception ex)\n" +
                "					{\n" +
                "						dt = DateTime.Now;\n" +
                "					}\n" +
                "\n" +
                "					strdate = dt.ToString(\"yyyy-MM-dd H:mm:ss\");\n" +
                "\n" +
                $"					{(KVP.ContainsKey(UserSid) ? $"args.Database.SetParameterIntValue(\"@user_sid\", {KVP[UserSid]});" : string.Empty)}\n" +
                "					args.Database.SetParameterIntValue(\"@active\", 1);\n" +
                "					args.Database.SetParameterIntValue(\"@product_info\", 13);\n" +
                "					args.Database.SetParameterIntValue(\"@listing_type\", 6);\n" +
                "                   \n" +
                $"{(KVP.ContainsKey(CustomKeywords1) ? $"                    args.Database.SetParameterTextValue(\"@{CustomKeywords1}\", \"{KVP[CustomKeywords1]}\");\n" : string.Empty)}" +
                $"{(KVP.ContainsKey(CustomKeywords2) ? $"                    args.Database.SetParameterTextValue(\"@{CustomKeywords2}\", \"{KVP[CustomKeywords2]}\");\n" : string.Empty)}" +
                $"{(KVP.ContainsKey(CustomKeywords3) ? $"                    args.Database.SetParameterTextValue(\"@{CustomKeywords3}\", \"{KVP[CustomKeywords3]}\");\n" : string.Empty)}" +
                $"{(KVP.ContainsKey(CustomKeywords4) ? $"                    args.Database.SetParameterTextValue(\"@{CustomKeywords4}\", \"{KVP[CustomKeywords4]}\");\n" : string.Empty)}" +
                "                   \n" +   
                $"{(KVP.ContainsKey(JobType) ? "                    args.Database.SetParameterTextValue(\"@jobtype\", GlobaljobsReader.GetStringValue(\"jobtype\"));\n" : string.Empty)}" +
                $"{(KVP.ContainsKey(JobId) ? "                    args.Database.SetParameterTextValue(\"@job_id\", GlobaljobsReader.GetStringValue(\"jobid\"));\n" : string.Empty)}" +
                "                    args.Database.SetParameterTextValue(\"@title\", GlobaljobsReader.GetStringValue(\"title\"));\n" +
                $"{(KVP.ContainsKey(JobLocation) ? "                    args.Database.SetParameterTextValue(\"@location\", GlobaljobsReader.GetStringValue(\"location_ciry\"));\n" : string.Empty)}" +
                $"{(KVP.ContainsKey(JobCategory) ? "                    args.Database.SetParameterTextValue(\"@JobCategory\", GlobaljobsReader.GetStringValue(\"category\"));\n" : string.Empty)}" +
                "					args.Database.SetParameterTextValue(\"@date\", strdate.ToString());\n" +
                "					args.Database.SetParameterTextValue(\"@description\", GlobaljobsReader.GetStringValue(\"description\"));\n" +
                "					args.Database.SetParameterTextValue(\"@application_url\", GlobaljobsReader.GetStringValue(\"apply\"));\n" +
                "\n" +
                "					args.Database.ExecuteNonQuery();\n" +
                "				}\n" +
                "			}\n" +
                "			catch (Exception exp)\n" +
                "			{\n" +
                "				args.WriteDebug(\"Custom inner script error: \" + exp.Message);\n" +
                "				run = false;\n" +
                "			}\n" +
                "			finally\n" +
                "			{\n" +
                "				GlobaljobsReader.Close();\n" +
                "			}\n" +
                "\n" +
                "			return run;\n" +
                "		}\n" +
                "		catch (Exception exp)\n" +
                "		{\n" +
                "			args.WriteDebug(\"Custom script error: \" + exp.Message);\n" +
                "			return false;\n" +
                "		}\n" +
                "	}\n" +
                "}";

            if (generationType == (int)GenerationType.CreateFile)
            {
                var dir = string.Empty;
                using (var fbd = new FolderBrowserDialog())
                {
                    var result = fbd.ShowDialog();

                    if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                        dir = $@"{fbd.SelectedPath}\{KVP[FileName]}_Script_{DateTime.Now:yyyy-MM-dd-H-mm-ss}.cs";
                }

                if (string.IsNullOrEmpty(dir) || string.IsNullOrWhiteSpace(dir))
                {
                    Clipboard.SetText(_scriptFileContent);
                    MessageBox.Show("Copied to clipboard but not saved\nYou must choose a location to save the file");
                    return;
                }

                File.WriteAllText(dir, _scriptFileContent);
            }
            else if (generationType == (int)GenerationType.CopyContent)
            {
                Clipboard.SetText(_scriptFileContent);
                MessageBox.Show("Copied to clipboard");
            }
        }

        private static string GenerateSQLQueryString(Dictionary<string, string> KVP)
        {
            return "insert into " +
                  $"listings(active, product_info, listing_type_sid, " +
                  $"{(KVP.ContainsKey(UserSid) ? UserSid + ", " : string.Empty)} " +
                  $"{(KVP.ContainsKey(JobId) ? JobId + ", " : string.Empty)} " +
                  $"{(KVP.ContainsKey(JobType) ? JobType + ", " : string.Empty)} title, JobDescription, " +
                  $"{(KVP.ContainsKey(JobCategory) ? JobCategory + ", " : string.Empty)} " +
                  $"{(KVP.ContainsKey(JobLocation) ? JobLocation + ", " : string.Empty)} activation_date, " +
                  $"{(KVP.ContainsKey(CustomKeywords1) ? CustomKeywords1 + ", " : string.Empty)} " +
                  $"{(KVP.ContainsKey(CustomKeywords2) ? CustomKeywords2 + ", " : string.Empty)} " +
                  $"{(KVP.ContainsKey(CustomKeywords3) ? CustomKeywords3 + ", " : string.Empty)} " +
                  $"{(KVP.ContainsKey(CustomKeywords4) ? CustomKeywords4 + ", " : string.Empty)} application_redirects) " +

                  $"values(@active, @product_info, @listing_type, " +
                  $"{(KVP.ContainsKey(UserSid) ? "@" + UserSid + ", " : string.Empty)} " +
                  $"{(KVP.ContainsKey(JobId) ? "@" + JobId + ", " : string.Empty)} " +
                  $"{(KVP.ContainsKey(JobType) ? "@" + JobType + ", " : string.Empty)} @title, @description, " +
                  $"{(KVP.ContainsKey(JobCategory) ? "@" + JobCategory + ", " : string.Empty)} " +
                  $"{(KVP.ContainsKey(JobLocation) ? "@" + JobLocation + ", " : string.Empty)} UNIX_TIMESTAMP(@date), " +
                  $"{(KVP.ContainsKey(CustomKeywords1) ? "@" + CustomKeywords1 + ", " : string.Empty)} " +
                  $"{(KVP.ContainsKey(CustomKeywords2) ? "@" + CustomKeywords2 + ", " : string.Empty)} " +
                  $"{(KVP.ContainsKey(CustomKeywords3) ? "@" + CustomKeywords3 + ", " : string.Empty)} " +
                  $"{(KVP.ContainsKey(CustomKeywords4) ? "@" + CustomKeywords4 + ", " : string.Empty)} @application_url)";
        }
    }
}