﻿namespace ITmaniacsScriptGenerator
{
    partial class Application
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Application));
            this.GenerateButton = new System.Windows.Forms.Button();
            this.CopyButton = new System.Windows.Forms.Button();
            this.X = new System.Windows.Forms.Button();
            this.MinimizeButton = new System.Windows.Forms.Button();
            this.CompanyNameLabel = new System.Windows.Forms.Label();
            this.CompanynNameTextBox = new System.Windows.Forms.TextBox();
            this.KeysPanel = new System.Windows.Forms.Panel();
            this.usersidnum = new System.Windows.Forms.NumericUpDown();
            this.categorycheck = new System.Windows.Forms.CheckBox();
            this.usersidcheck = new System.Windows.Forms.CheckBox();
            this.customckeck1 = new System.Windows.Forms.CheckBox();
            this.customckeck2 = new System.Windows.Forms.CheckBox();
            this.customckeck3 = new System.Windows.Forms.CheckBox();
            this.customckeck4 = new System.Windows.Forms.CheckBox();
            this.jobtypecheck = new System.Windows.Forms.CheckBox();
            this.jobidcheck = new System.Windows.Forms.CheckBox();
            this.locationcheck = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.customtextbox4 = new System.Windows.Forms.TextBox();
            this.CustomKeyword4 = new System.Windows.Forms.Label();
            this.customtextbox3 = new System.Windows.Forms.TextBox();
            this.CustomKeyword3 = new System.Windows.Forms.Label();
            this.customtextbox2 = new System.Windows.Forms.TextBox();
            this.CustomKeyword2 = new System.Windows.Forms.Label();
            this.customtextbox1 = new System.Windows.Forms.TextBox();
            this.CustomKeyword1 = new System.Windows.Forms.Label();
            this.SIDLabel = new System.Windows.Forms.Label();
            this.FileNametextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.KeysPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.usersidnum)).BeginInit();
            this.SuspendLayout();
            // 
            // GenerateButton
            // 
            this.GenerateButton.BackColor = System.Drawing.Color.Transparent;
            this.GenerateButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.GenerateButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.GenerateButton.FlatAppearance.BorderSize = 0;
            this.GenerateButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.GenerateButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.GenerateButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.GenerateButton.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.GenerateButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.GenerateButton.Location = new System.Drawing.Point(185, 323);
            this.GenerateButton.Margin = new System.Windows.Forms.Padding(2);
            this.GenerateButton.Name = "GenerateButton";
            this.GenerateButton.Size = new System.Drawing.Size(184, 33);
            this.GenerateButton.TabIndex = 0;
            this.GenerateButton.Text = "Generate";
            this.GenerateButton.UseVisualStyleBackColor = false;
            this.GenerateButton.Click += new System.EventHandler(this.GenerateButton_Click);
            // 
            // CopyButton
            // 
            this.CopyButton.BackColor = System.Drawing.Color.Transparent;
            this.CopyButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CopyButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.CopyButton.FlatAppearance.BorderSize = 0;
            this.CopyButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.CopyButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.CopyButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CopyButton.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.CopyButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.CopyButton.Location = new System.Drawing.Point(185, 272);
            this.CopyButton.Margin = new System.Windows.Forms.Padding(2);
            this.CopyButton.Name = "CopyButton";
            this.CopyButton.Size = new System.Drawing.Size(184, 33);
            this.CopyButton.TabIndex = 1;
            this.CopyButton.Text = "Copy";
            this.CopyButton.UseVisualStyleBackColor = false;
            this.CopyButton.Click += new System.EventHandler(this.CopyButton_Click);
            // 
            // X
            // 
            this.X.BackColor = System.Drawing.Color.Transparent;
            this.X.Cursor = System.Windows.Forms.Cursors.Hand;
            this.X.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.X.FlatAppearance.BorderSize = 0;
            this.X.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.X.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.X.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.X.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.X.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.X.Location = new System.Drawing.Point(364, 4);
            this.X.Margin = new System.Windows.Forms.Padding(2);
            this.X.Name = "X";
            this.X.Size = new System.Drawing.Size(30, 30);
            this.X.TabIndex = 2;
            this.X.Text = "X";
            this.X.UseVisualStyleBackColor = false;
            this.X.Click += new System.EventHandler(this.X_Click);
            // 
            // MinimizeButton
            // 
            this.MinimizeButton.BackColor = System.Drawing.Color.Transparent;
            this.MinimizeButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MinimizeButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.MinimizeButton.FlatAppearance.BorderSize = 0;
            this.MinimizeButton.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.MinimizeButton.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(60)))), ((int)(((byte)(60)))));
            this.MinimizeButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.MinimizeButton.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.MinimizeButton.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.MinimizeButton.Location = new System.Drawing.Point(331, 3);
            this.MinimizeButton.Margin = new System.Windows.Forms.Padding(2);
            this.MinimizeButton.Name = "MinimizeButton";
            this.MinimizeButton.Size = new System.Drawing.Size(30, 30);
            this.MinimizeButton.TabIndex = 3;
            this.MinimizeButton.Text = "_";
            this.MinimizeButton.UseVisualStyleBackColor = false;
            this.MinimizeButton.Click += new System.EventHandler(this.Minimize_Click);
            // 
            // CompanyNameLabel
            // 
            this.CompanyNameLabel.AutoSize = true;
            this.CompanyNameLabel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.CompanyNameLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.CompanyNameLabel.Location = new System.Drawing.Point(53, 50);
            this.CompanyNameLabel.Name = "CompanyNameLabel";
            this.CompanyNameLabel.Size = new System.Drawing.Size(108, 19);
            this.CompanyNameLabel.TabIndex = 0;
            this.CompanyNameLabel.Text = "Company Name";
            // 
            // CompanynNameTextBox
            // 
            this.CompanynNameTextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.CompanynNameTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CompanynNameTextBox.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.CompanynNameTextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.CompanynNameTextBox.Location = new System.Drawing.Point(223, 50);
            this.CompanynNameTextBox.Name = "CompanynNameTextBox";
            this.CompanynNameTextBox.Size = new System.Drawing.Size(157, 23);
            this.CompanynNameTextBox.TabIndex = 0;
            // 
            // KeysPanel
            // 
            this.KeysPanel.AutoScroll = true;
            this.KeysPanel.Controls.Add(this.FileNametextBox);
            this.KeysPanel.Controls.Add(this.label5);
            this.KeysPanel.Controls.Add(this.usersidnum);
            this.KeysPanel.Controls.Add(this.categorycheck);
            this.KeysPanel.Controls.Add(this.usersidcheck);
            this.KeysPanel.Controls.Add(this.customckeck1);
            this.KeysPanel.Controls.Add(this.customckeck2);
            this.KeysPanel.Controls.Add(this.customckeck3);
            this.KeysPanel.Controls.Add(this.customckeck4);
            this.KeysPanel.Controls.Add(this.jobtypecheck);
            this.KeysPanel.Controls.Add(this.GenerateButton);
            this.KeysPanel.Controls.Add(this.CopyButton);
            this.KeysPanel.Controls.Add(this.jobidcheck);
            this.KeysPanel.Controls.Add(this.locationcheck);
            this.KeysPanel.Controls.Add(this.label1);
            this.KeysPanel.Controls.Add(this.label2);
            this.KeysPanel.Controls.Add(this.label3);
            this.KeysPanel.Controls.Add(this.label4);
            this.KeysPanel.Controls.Add(this.customtextbox4);
            this.KeysPanel.Controls.Add(this.CustomKeyword4);
            this.KeysPanel.Controls.Add(this.customtextbox3);
            this.KeysPanel.Controls.Add(this.CustomKeyword3);
            this.KeysPanel.Controls.Add(this.customtextbox2);
            this.KeysPanel.Controls.Add(this.CustomKeyword2);
            this.KeysPanel.Controls.Add(this.customtextbox1);
            this.KeysPanel.Controls.Add(this.CustomKeyword1);
            this.KeysPanel.Location = new System.Drawing.Point(11, 39);
            this.KeysPanel.Name = "KeysPanel";
            this.KeysPanel.Size = new System.Drawing.Size(378, 387);
            this.KeysPanel.TabIndex = 4;
            // 
            // usersidnum
            // 
            this.usersidnum.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.usersidnum.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.usersidnum.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.usersidnum.Location = new System.Drawing.Point(213, 43);
            this.usersidnum.Name = "usersidnum";
            this.usersidnum.Size = new System.Drawing.Size(50, 25);
            this.usersidnum.TabIndex = 7;
            // 
            // categorycheck
            // 
            this.categorycheck.AutoSize = true;
            this.categorycheck.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.categorycheck.Checked = true;
            this.categorycheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.categorycheck.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.categorycheck.FlatAppearance.BorderSize = 3;
            this.categorycheck.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.categorycheck.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.categorycheck.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.categorycheck.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.categorycheck.Location = new System.Drawing.Point(20, 340);
            this.categorycheck.Name = "categorycheck";
            this.categorycheck.Size = new System.Drawing.Size(12, 11);
            this.categorycheck.TabIndex = 28;
            this.categorycheck.UseVisualStyleBackColor = true;
            // 
            // usersidcheck
            // 
            this.usersidcheck.AutoSize = true;
            this.usersidcheck.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.usersidcheck.Checked = true;
            this.usersidcheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.usersidcheck.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.usersidcheck.FlatAppearance.BorderSize = 3;
            this.usersidcheck.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.usersidcheck.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.usersidcheck.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.usersidcheck.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.usersidcheck.Location = new System.Drawing.Point(20, 47);
            this.usersidcheck.Name = "usersidcheck";
            this.usersidcheck.Size = new System.Drawing.Size(12, 11);
            this.usersidcheck.TabIndex = 27;
            this.usersidcheck.UseVisualStyleBackColor = true;
            // 
            // customckeck1
            // 
            this.customckeck1.AutoSize = true;
            this.customckeck1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.customckeck1.Checked = true;
            this.customckeck1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.customckeck1.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.customckeck1.FlatAppearance.BorderSize = 3;
            this.customckeck1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.customckeck1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.customckeck1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.customckeck1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.customckeck1.Location = new System.Drawing.Point(20, 80);
            this.customckeck1.Name = "customckeck1";
            this.customckeck1.Size = new System.Drawing.Size(12, 11);
            this.customckeck1.TabIndex = 26;
            this.customckeck1.UseVisualStyleBackColor = true;
            // 
            // customckeck2
            // 
            this.customckeck2.AutoSize = true;
            this.customckeck2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.customckeck2.Checked = true;
            this.customckeck2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.customckeck2.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.customckeck2.FlatAppearance.BorderSize = 3;
            this.customckeck2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.customckeck2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.customckeck2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.customckeck2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.customckeck2.Location = new System.Drawing.Point(20, 113);
            this.customckeck2.Name = "customckeck2";
            this.customckeck2.Size = new System.Drawing.Size(12, 11);
            this.customckeck2.TabIndex = 25;
            this.customckeck2.UseVisualStyleBackColor = true;
            // 
            // customckeck3
            // 
            this.customckeck3.AutoSize = true;
            this.customckeck3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.customckeck3.Checked = true;
            this.customckeck3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.customckeck3.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.customckeck3.FlatAppearance.BorderSize = 3;
            this.customckeck3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.customckeck3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.customckeck3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.customckeck3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.customckeck3.Location = new System.Drawing.Point(20, 145);
            this.customckeck3.Name = "customckeck3";
            this.customckeck3.Size = new System.Drawing.Size(12, 11);
            this.customckeck3.TabIndex = 24;
            this.customckeck3.UseVisualStyleBackColor = true;
            // 
            // customckeck4
            // 
            this.customckeck4.AutoSize = true;
            this.customckeck4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.customckeck4.Checked = true;
            this.customckeck4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.customckeck4.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.customckeck4.FlatAppearance.BorderSize = 3;
            this.customckeck4.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.customckeck4.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.customckeck4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.customckeck4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.customckeck4.Location = new System.Drawing.Point(20, 178);
            this.customckeck4.Name = "customckeck4";
            this.customckeck4.Size = new System.Drawing.Size(12, 11);
            this.customckeck4.TabIndex = 23;
            this.customckeck4.UseVisualStyleBackColor = true;
            // 
            // jobtypecheck
            // 
            this.jobtypecheck.AutoSize = true;
            this.jobtypecheck.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.jobtypecheck.Checked = true;
            this.jobtypecheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.jobtypecheck.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.jobtypecheck.FlatAppearance.BorderSize = 3;
            this.jobtypecheck.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.jobtypecheck.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.jobtypecheck.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.jobtypecheck.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.jobtypecheck.Location = new System.Drawing.Point(20, 242);
            this.jobtypecheck.Name = "jobtypecheck";
            this.jobtypecheck.Size = new System.Drawing.Size(12, 11);
            this.jobtypecheck.TabIndex = 22;
            this.jobtypecheck.UseVisualStyleBackColor = true;
            // 
            // jobidcheck
            // 
            this.jobidcheck.AutoSize = true;
            this.jobidcheck.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.jobidcheck.Checked = true;
            this.jobidcheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.jobidcheck.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.jobidcheck.FlatAppearance.BorderSize = 3;
            this.jobidcheck.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.jobidcheck.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.jobidcheck.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.jobidcheck.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.jobidcheck.Location = new System.Drawing.Point(20, 275);
            this.jobidcheck.Name = "jobidcheck";
            this.jobidcheck.Size = new System.Drawing.Size(12, 11);
            this.jobidcheck.TabIndex = 21;
            this.jobidcheck.UseVisualStyleBackColor = true;
            // 
            // locationcheck
            // 
            this.locationcheck.AutoSize = true;
            this.locationcheck.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.locationcheck.Checked = true;
            this.locationcheck.CheckState = System.Windows.Forms.CheckState.Checked;
            this.locationcheck.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.locationcheck.FlatAppearance.BorderSize = 3;
            this.locationcheck.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(70)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.locationcheck.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(80)))), ((int)(((byte)(80)))), ((int)(((byte)(80)))));
            this.locationcheck.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.locationcheck.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.locationcheck.Location = new System.Drawing.Point(20, 307);
            this.locationcheck.Name = "locationcheck";
            this.locationcheck.Size = new System.Drawing.Size(12, 11);
            this.locationcheck.TabIndex = 20;
            this.locationcheck.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.label1.Location = new System.Drawing.Point(45, 336);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 19);
            this.label1.TabIndex = 18;
            this.label1.Text = "Category";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.label2.Location = new System.Drawing.Point(44, 303);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 19);
            this.label2.TabIndex = 17;
            this.label2.Text = "Location";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.label3.Location = new System.Drawing.Point(44, 271);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(46, 19);
            this.label3.TabIndex = 16;
            this.label3.Text = "Job Id";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.label4.Location = new System.Drawing.Point(44, 238);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 19);
            this.label4.TabIndex = 15;
            this.label4.Text = "Job Type";
            // 
            // customtextbox4
            // 
            this.customtextbox4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.customtextbox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.customtextbox4.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.customtextbox4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.customtextbox4.Location = new System.Drawing.Point(214, 174);
            this.customtextbox4.Name = "customtextbox4";
            this.customtextbox4.Size = new System.Drawing.Size(157, 23);
            this.customtextbox4.TabIndex = 13;
            // 
            // CustomKeyword4
            // 
            this.CustomKeyword4.AutoSize = true;
            this.CustomKeyword4.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.CustomKeyword4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.CustomKeyword4.Location = new System.Drawing.Point(44, 174);
            this.CustomKeyword4.Name = "CustomKeyword4";
            this.CustomKeyword4.Size = new System.Drawing.Size(126, 19);
            this.CustomKeyword4.TabIndex = 14;
            this.CustomKeyword4.Text = "Custom Keyword 4";
            // 
            // customtextbox3
            // 
            this.customtextbox3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.customtextbox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.customtextbox3.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.customtextbox3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.customtextbox3.Location = new System.Drawing.Point(213, 141);
            this.customtextbox3.Name = "customtextbox3";
            this.customtextbox3.Size = new System.Drawing.Size(157, 23);
            this.customtextbox3.TabIndex = 11;
            // 
            // CustomKeyword3
            // 
            this.CustomKeyword3.AutoSize = true;
            this.CustomKeyword3.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.CustomKeyword3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.CustomKeyword3.Location = new System.Drawing.Point(43, 141);
            this.CustomKeyword3.Name = "CustomKeyword3";
            this.CustomKeyword3.Size = new System.Drawing.Size(126, 19);
            this.CustomKeyword3.TabIndex = 12;
            this.CustomKeyword3.Text = "Custom Keyword 3";
            // 
            // customtextbox2
            // 
            this.customtextbox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.customtextbox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.customtextbox2.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.customtextbox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.customtextbox2.Location = new System.Drawing.Point(213, 109);
            this.customtextbox2.Name = "customtextbox2";
            this.customtextbox2.Size = new System.Drawing.Size(157, 23);
            this.customtextbox2.TabIndex = 9;
            // 
            // CustomKeyword2
            // 
            this.CustomKeyword2.AutoSize = true;
            this.CustomKeyword2.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.CustomKeyword2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.CustomKeyword2.Location = new System.Drawing.Point(43, 109);
            this.CustomKeyword2.Name = "CustomKeyword2";
            this.CustomKeyword2.Size = new System.Drawing.Size(126, 19);
            this.CustomKeyword2.TabIndex = 10;
            this.CustomKeyword2.Text = "Custom Keyword 2";
            // 
            // customtextbox1
            // 
            this.customtextbox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.customtextbox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.customtextbox1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.customtextbox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.customtextbox1.Location = new System.Drawing.Point(213, 76);
            this.customtextbox1.Name = "customtextbox1";
            this.customtextbox1.Size = new System.Drawing.Size(157, 23);
            this.customtextbox1.TabIndex = 7;
            // 
            // CustomKeyword1
            // 
            this.CustomKeyword1.AutoSize = true;
            this.CustomKeyword1.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.CustomKeyword1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.CustomKeyword1.Location = new System.Drawing.Point(43, 76);
            this.CustomKeyword1.Name = "CustomKeyword1";
            this.CustomKeyword1.Size = new System.Drawing.Size(126, 19);
            this.CustomKeyword1.TabIndex = 8;
            this.CustomKeyword1.Text = "Custom Keyword 1";
            // 
            // SIDLabel
            // 
            this.SIDLabel.AutoSize = true;
            this.SIDLabel.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.SIDLabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.SIDLabel.Location = new System.Drawing.Point(53, 82);
            this.SIDLabel.Name = "SIDLabel";
            this.SIDLabel.Size = new System.Drawing.Size(59, 19);
            this.SIDLabel.TabIndex = 6;
            this.SIDLabel.Text = "User Sid";
            // 
            // FileNametextBox
            // 
            this.FileNametextBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.FileNametextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.FileNametextBox.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.FileNametextBox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.FileNametextBox.Location = new System.Drawing.Point(214, 207);
            this.FileNametextBox.Name = "FileNametextBox";
            this.FileNametextBox.Size = new System.Drawing.Size(157, 23);
            this.FileNametextBox.TabIndex = 29;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(235)))), ((int)(((byte)(20)))));
            this.label5.Location = new System.Drawing.Point(44, 207);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 19);
            this.label5.TabIndex = 30;
            this.label5.Text = "FileName";
            // 
            // Application
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(40)))), ((int)(((byte)(40)))), ((int)(((byte)(40)))));
            this.ClientSize = new System.Drawing.Size(403, 441);
            this.Controls.Add(this.SIDLabel);
            this.Controls.Add(this.CompanynNameTextBox);
            this.Controls.Add(this.CompanyNameLabel);
            this.Controls.Add(this.KeysPanel);
            this.Controls.Add(this.MinimizeButton);
            this.Controls.Add(this.X);
            this.Font = new System.Drawing.Font("Segoe UI", 8F);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Application";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ITmaniacs Script Generator";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Application_MouseDown);
            this.KeysPanel.ResumeLayout(false);
            this.KeysPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.usersidnum)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button GenerateButton;
        private System.Windows.Forms.Button CopyButton;
        private System.Windows.Forms.Button X;
        private System.Windows.Forms.Button MinimizeButton;
        private System.Windows.Forms.Label CompanyNameLabel;
        private System.Windows.Forms.TextBox CompanynNameTextBox;
        private System.Windows.Forms.Panel KeysPanel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox customtextbox4;
        private System.Windows.Forms.Label CustomKeyword4;
        private System.Windows.Forms.TextBox customtextbox3;
        private System.Windows.Forms.Label CustomKeyword3;
        private System.Windows.Forms.TextBox customtextbox2;
        private System.Windows.Forms.Label CustomKeyword2;
        private System.Windows.Forms.TextBox customtextbox1;
        private System.Windows.Forms.Label CustomKeyword1;
        private System.Windows.Forms.Label SIDLabel;
        private System.Windows.Forms.CheckBox categorycheck;
        private System.Windows.Forms.CheckBox customckeck1;
        private System.Windows.Forms.CheckBox customckeck2;
        private System.Windows.Forms.CheckBox customckeck3;
        private System.Windows.Forms.CheckBox customckeck4;
        private System.Windows.Forms.CheckBox jobtypecheck;
        private System.Windows.Forms.CheckBox jobidcheck;
        private System.Windows.Forms.CheckBox locationcheck;
        private System.Windows.Forms.NumericUpDown usersidnum;
        private System.Windows.Forms.CheckBox usersidcheck;
        private System.Windows.Forms.TextBox FileNametextBox;
        private System.Windows.Forms.Label label5;
    }
}

