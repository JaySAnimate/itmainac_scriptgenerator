﻿using System;
using System.Windows.Forms;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace ITmaniacsScriptGenerator
{
    public partial class Application : Form
    {
        private readonly Dictionary<string, string> KVP = new Dictionary<string, string>();

        private const int WM_NCLBUTTONDOWN = 0xA1;
        private const int HT_CAPTION = 0x2;

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImport("user32.dll")]
        public static extern bool ReleaseCapture();

        public Application() => InitializeComponent();

        private void X_Click(object sender, EventArgs e) => Environment.Exit(0);

        private void Minimize_Click(object sender, EventArgs e) => WindowState = FormWindowState.Minimized;

        private void GenerateButton_Click(object sender, EventArgs e)
        {
            GenerateButton.Enabled = false;
            Generate((int)GenerationType.CreateFile);
            GenerateButton.Enabled = true;
        }

        private void CopyButton_Click(object sender, EventArgs e)
        {
            CopyButton.Enabled = false;
            Generate((int)GenerationType.CopyContent);
            CopyButton.Enabled = true;
        }

        private void Application_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void Generate(int generationType)
        {
            if (string.IsNullOrEmpty(CompanynNameTextBox.Text))
            {
                MessageBox.Show("Company Name cannot be empty");
                GenerateButton.Enabled = true;
                return;
            }

            InitializeKVP();

            Generator.GenerateScript(KVP, generationType);

            KVP.Clear();
        }

        private void InitializeKVP()
        {
            KVP[Generator.Company_Name] = CompanynNameTextBox.Text;
            KVP[Generator.FileName] = FileNametextBox.Text;

            if (usersidcheck.Checked)
                KVP[Generator.UserSid] = usersidnum.Value.ToString();

            if (customckeck1.Checked && !string.IsNullOrEmpty(customtextbox1.Text) && !string.IsNullOrWhiteSpace(customtextbox1.Text))
                KVP[Generator.CustomKeywords1] = customtextbox1.Text;

            if (customckeck2.Checked && !string.IsNullOrEmpty(customtextbox2.Text) && !string.IsNullOrWhiteSpace(customtextbox2.Text))
                KVP[Generator.CustomKeywords2] = customtextbox2.Text;

            if (customckeck3.Checked && !string.IsNullOrEmpty(customtextbox3.Text) && !string.IsNullOrWhiteSpace(customtextbox3.Text))
                KVP[Generator.CustomKeywords3] = customtextbox3.Text;

            if (customckeck4.Checked && !string.IsNullOrEmpty(customtextbox4.Text) && !string.IsNullOrWhiteSpace(customtextbox4.Text))
                KVP[Generator.CustomKeywords4] = customtextbox4.Text;

            if (jobtypecheck.Checked)
                KVP[Generator.JobType] = jobtypecheck.Checked.ToString();

            if (jobidcheck.Checked)
                KVP[Generator.JobId] = jobidcheck.Checked.ToString();

            if (locationcheck.Checked)
                KVP[Generator.JobLocation] = locationcheck.Checked.ToString();

            if (categorycheck.Checked)
                KVP[Generator.JobCategory] = categorycheck.Checked.ToString();
        }
    }
}